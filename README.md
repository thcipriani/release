# Release tools

A group of tools used for releasing code.

It includes:

- Building the MediaWiki container image
- Creating MediaWiki release notes
- Creating a new release branch for MediaWiki

And small utilities for:

- Updating the deployment calendar on Wikitech
- Creating new train tasks
